#Tabs Challenge

![alt stewie loves tab](https://media.giphy.com/media/sCt3yFcIb7hWU/giphy.gif)

## Live Link

CLICK HERE TO VIEW LIVE SITE : [LIVE URL](https://tabs-challenge.netlify.com/)

### Running Application

`yarn install && yarn start`

### Testing Application

`yarn test`

##### TECHNOLOGIES

- React, Hooks + Functional Components
- Hand Configured Webpack
- Jest + React Testing Library Tests
- Eslint: Airbnb, React, Security, Accessibility
- Prettier config
- Husky: pre-commit hooks
- Emotion: css in javascript, all styling
- Fetch + Fetch Mock for API calls and Testing

---

### App Details

Find the app entry here: [Tabs](https://bitbucket.org/tracymcaruso/tabchallenge/src/18ffb0670c50b5952cb4b70e5ef74ec36a7deb3e/src/components/Tabs.js?at=master)

##### STATE

Component state is modified using the useState and useEffect hooks

**tabsData**: List of Tabs

Tabs are fetched from a simple JSON store, served from myjson.com

**activeTab**: Currently selected Tab

Active tab defaults to 0 and is updated on user Click or Keypress

##### STRUCTURE

The tab component is composed of a navigation and content section. The navigation component can be used to tab, scroll or navigate your way through the tabs available. The content section is updated on on user Click or Keypress.

---

##### IMPROVEMENTS

- Transition animations
- Create top level CSS variable file to store all repeated vars
- Improve navigation tabbing
- Add more accessibility features
- Create backend API to serve tab data
- Reorganize folder structure as application grows
