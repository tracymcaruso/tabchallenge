import React, { useState, useEffect, useRef } from 'react';
import Container from 'Components/Container';
import Nav from 'Components/Nav';
import TabDirection from 'Components/TabDirection';
import Title from 'Components/Title';
import Content from 'Components/Content';
import { ScrollTo } from 'react-scroll-to';

const Tabs = () => {
  const [allTabs, setAllTabs] = useState([]);
  const [activeTab, setActiveTab] = useState({
    title: '',
    id: 0,
    content: '',
    url: '',
  });

  const fetchTabs = async () => {
    const res = await fetch('https://api.myjson.com/bins/9ebj4');
    const {
      tabs,
      tabs: [first],
    } = await res.json();

    setAllTabs(tabs);
    setActiveTab(first);
  };

  useEffect(() => {
    fetchTabs();
  }, []);

  const refDOM = useRef();

  const handleForward = (scrollTo, ref) => {
    scrollTo({
      ref: ref.firstElementChild,
      x: ref.getBoundingClientRect().width,
      smooth: true,
    });
  };

  const handleReverse = (scrollTo, ref) => {
    scrollTo({
      ref: ref.firstElementChild,
      x: -ref.getBoundingClientRect().width,
      smooth: true,
    });
  };

  const handleTabChange = tab => {
    setActiveTab(tab);
  };

  if (allTabs < 1) return <h1 data-testid="loading">Loading</h1>;
  return (
    <Container>
      <ScrollTo>
        {({ scrollTo }) => (
          <>
            <TabDirection
              direction="prev"
              handleTabDirection={() => handleReverse(scrollTo, refDOM.current)}
            />
            <TabDirection
              direction="next"
              handleTabDirection={() => handleForward(scrollTo, refDOM.current)}
            />
          </>
        )}
      </ScrollTo>
      <div ref={refDOM}>
        <Nav>
          {allTabs.map(tab => (
            <Title
              key={tab.id}
              tab={tab}
              isActive={tab === activeTab}
              handleTabChange={() => handleTabChange(tab)}
            />
          ))}
        </Nav>
      </div>
      <Content activeTab={activeTab} />
    </Container>
  );
};

export default Tabs;
