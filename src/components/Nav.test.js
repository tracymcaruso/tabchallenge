/* eslint-disable no-console */
import React from 'react';
import { render, cleanup } from 'react-testing-library';

import Nav from 'Components/Nav';

afterEach(() => {
  cleanup();
  console.error.mockClear();
});

// eslint-disable-next-line no-undef
console.error = jest.fn();

test('<Nav />, without an array of children', () => {
  render(<Nav />);
  expect(console.error).toHaveBeenCalled();
  render(
    <Nav>
      <li />
    </Nav>,
  );
  expect(console.error).toHaveBeenCalled();
});

test('<Nav />', () => {
  const { getByTestId } = render(
    <Nav>
      <li />
      <li />
    </Nav>,
  );
  const nav = getByTestId('nav');
  expect(nav.tagName).toBe('UL');
});
