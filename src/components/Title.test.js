/* eslint-disable no-console */
import React from 'react';
import { render, cleanup } from 'react-testing-library';

import Title from 'Components/Title';

afterEach(() => {
  cleanup();
  console.error.mockClear();
});

// eslint-disable-next-line no-undef
console.error = jest.fn(() => {});

// eslint-disable-next-line no-undef
const stubTabChange = jest.fn();

const tab = { title: 'test 0', content: 'test content 0', id: 0 };

test('<Title />, without tab', () => {
  render(<Title handleTabChange={() => stubTabChange(tab)} />);
  expect(console.error).toHaveBeenCalled();
});

test('<Title />, with wrong props', () => {
  render(
    <Title
      tab={{ other: 'data' }}
      handleTabChange={() => stubTabChange(tab)}
    />,
  );
  expect(console.error).toHaveBeenCalled();
});

test('<Title />, without click handler', () => {
  render(<Title tab={tab} />);
  expect(console.error).toHaveBeenCalled();
});

test('<Title />', () => {
  const { getByTestId } = render(
    <Title tab={tab} handleTabChange={() => stubTabChange(tab)} />,
  );
  const title = getByTestId('title');
  expect(title.tagName).toBe('BUTTON');
  expect(title.textContent).toBe(tab.title);
});
