import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const TabDirection = ({ direction, handleTabDirection }) => {
  return (
    <Direction
      onClick={handleTabDirection}
      onKeyDown={handleTabDirection}
      style={direction === 'prev' ? { left: '20px' } : { right: '20px' }}
      data-testid="tab-direction"
      type="button"
      role="button"
    >
      {direction === 'prev' ? '<' : '>'}
    </Direction>
  );
};

const Direction = styled('button')`
  background: rgb(116, 122, 126, 0);
  border-radius: 5px;
  border: none;
  color: rgb(116, 122, 126, 0);
  height: 66px;
  outline: none;
  position: absolute;
  top: 20px;
  width: 44px;
  z-index: 10;

  &:hover {
    background: rgb(116, 122, 126, 1);
    color: #fff;
    display: block;
  }
`;

TabDirection.propTypes = {
  direction: PropTypes.string.isRequired,
  handleTabDirection: PropTypes.func.isRequired,
};

export default TabDirection;
