/* eslint-disable no-console */
import React from 'react';
import {
  render,
  cleanup,
  waitForElement,
  fireEvent,
} from 'react-testing-library';

import Tabs from 'Components/Tabs';

global.fetch = require('jest-fetch-mock');

afterEach(() => {
  cleanup();
});

// eslint-disable-next-line no-undef
console.error = jest.fn();

const data = {
  tabs: [
    { title: 'test 0', content: 'test content 0', id: 0 },
    { title: 'test 1', content: 'test content 1', id: 1 },
    { title: 'test 2', content: 'test content 2', id: 2 },
  ],
};

test('<Tabs />', async () => {
  fetch.mockResponseOnce(JSON.stringify(data));
  const { queryByTestId, getAllByTestId, getByTestId } = render(<Tabs />);
  expect(queryByTestId('loading')).toBeTruthy();
  await waitForElement(() => getAllByTestId('container'));
  expect(queryByTestId('container')).toBeTruthy();
  expect(queryByTestId('nav')).toBeTruthy();
  const titles = getAllByTestId('title');
  expect(titles.length).toBe(3);
  const [titleOne, titleTwo, titleThree] = titles;
  const {
    tabs: [
      { content: contentOne },
      { content: contentTwo },
      { content: contentThree },
    ],
  } = data;

  expect(getByTestId('content').textContent).toBe(contentOne);
  fireEvent.click(titleTwo);
  expect(getByTestId('content').textContent).toBe(contentTwo);
  fireEvent.click(titleThree);
  expect(getByTestId('content').textContent).toBe(contentThree);
  fireEvent.click(titleOne);
  expect(getByTestId('content').textContent).toBe(contentOne);
});
