/* eslint-disable no-console */
import React from 'react';
import { render, cleanup } from 'react-testing-library';

import Container from 'Components/Container';

afterEach(() => {
  cleanup();
  console.error.mockClear();
});

// eslint-disable-next-line no-undef
console.error = jest.fn();

test('<Container />, without children', () => {
  render(<Container />);
  expect(console.error).toHaveBeenCalled();
});

test('<Container />', () => {
  const { getByTestId } = render(<Container>Child</Container>);
  const container = getByTestId('container');
  expect(container.tagName).toBe('SECTION');
});
