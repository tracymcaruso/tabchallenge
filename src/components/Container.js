import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const Container = props => {
  if (!props) return null;
  return <TabsContainer className="tabs" data-testid="container" {...props} />;
};

Container.propTypes = {
  children: PropTypes.node.isRequired,
};

const TabsContainer = styled('section')`
  border: 1px solid rgb(221, 221, 221);
  display: block;
  font-size: 16px;
  height: 665px;
  line-height: 24px;
  overflow-wrap: break-word;
  max-width: 760px;
  position: relative;
  overflow: auto;
`;

export default Container;
