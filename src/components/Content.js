import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const Content = ({ activeTab }) => {
  if (!activeTab) return null;
  const { content, url, title } = activeTab;
  return (
    <TabContent className="content" data-testid="content">
      <TabText>{content}</TabText>
      <TabImage src={url} alt={title} />
    </TabContent>
  );
};

const TabContent = styled('div')`
  color: rgb(49, 53, 55);
  font-family: Merriweather;
  font-size: 16px;
  font-weight: 300;
  height: 570px;
  overflow: scroll;
  padding: 0px 30px;
  max-width: 760px;
`;

const TabText = styled('p')`
  padding: 5px 0px;
`;

const TabImage = styled('img')`
  width: 100%;
`;

Content.propTypes = {
  activeTab: PropTypes.shape({
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
  }).isRequired,
};

export default Content;
