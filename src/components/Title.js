import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import truncate from '../utils/truncate';

const TITLE_LENGTH = 25;

const Title = ({ tab, isActive, handleTabChange }) => {
  if (!tab) return null;
  return (
    <TabTitle>
      <TabButton
        data-testid="title"
        onClick={handleTabChange}
        onKeyDown={handleTabChange}
        className={isActive ? 'active' : ''}
        type="button"
      >
        {typeof tab.title !== 'undefined' && tab.title.length > TITLE_LENGTH
          ? `${truncate(tab.title, TITLE_LENGTH)}...`
          : `${tab.title}`}
      </TabButton>
    </TabTitle>
  );
};

const TabTitle = styled('li')`
  align-items: center;
  color: rgb(49, 53, 55);
  display: flex;
  justify-content: center;
  min-width: 23%;
`;

const TabButton = styled('button')`
  background: rgb(245, 245, 245);
  border: 1px solid rgb(221, 221, 221);
  border-left: none;
  border-top: none;
  cursor: pointer;
  font-family: 'Lato', sans-serif;
  font-size: 12px;
  height: 100%;
  letter-spacing: 1.44px;
  line-heigt: 20px;
  outline: none;
  text-overflow: ellipsis;
  text-transform: uppercase;
  overflow-wrap: break-word;
  width: 100%;
  &.active {
    background: rgb(255, 255, 255);
    border-bottom: none;
    color: rgb(116, 122, 126);
    transition: background 0.04s, border 0.04s;
  }
`;

Title.propTypes = {
  tab: PropTypes.shape({
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
  }).isRequired,
  isActive: PropTypes.bool.isRequired,
  handleTabChange: PropTypes.func.isRequired,
};

export default Title;
