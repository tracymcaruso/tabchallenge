import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const Nav = ({ ...props }) => {
  if (!props) return null;
  return <TabsNav className="nav" data-testid="nav" {...props} />;
};

const TabsNav = styled('ul')`
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  border-right: 1px solid rgb(221, 221, 221);
  display: flex;
  flex-wrap: nowrap;
  height: 80px;
  margin: 0px;
  margin-right: -1px;
  overflow-wrap: break-word;
  overflow-y: hidden;
  padding-left: 0px;
  position: relative;
`;

Nav.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
};

export default Nav;
