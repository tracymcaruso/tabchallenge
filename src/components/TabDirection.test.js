/* eslint-disable no-console */
import React from 'react';
import { render, cleanup } from 'react-testing-library';

import TabDirection from 'Components/TabDirection';

afterEach(() => {
  cleanup();
  console.error.mockClear();
});

// eslint-disable-next-line no-undef
console.error = jest.fn(() => {});

// eslint-disable-next-line no-undef
const stubTabDirection = jest.fn();

const tab = { title: 'test 0', content: 'test content 0', id: 0 };

test('<TabDirection />, without direction', () => {
  render(<TabDirection handleTabDirection={() => stubTabDirection(tab)} />);
  expect(console.error).toHaveBeenCalled();
});

test('<TabDirection />, without click handler', () => {
  render(<TabDirection direction="prev" />);
  expect(console.error).toHaveBeenCalled();
});

test('<TabDirection />, prev', () => {
  const { getByTestId } = render(
    <TabDirection
      direction="prev"
      handleTabDirection={() => stubTabDirection(tab)}
    />,
  );
  const direction = getByTestId('tab-direction');
  expect(direction.tagName).toBe('BUTTON');
  expect(direction.textContent).toBe('<');
});

test('<TabDirection />, next', () => {
  const { getByTestId } = render(
    <TabDirection
      direction="next"
      handleTabDirection={() => stubTabDirection(tab)}
    />,
  );
  const direction = getByTestId('tab-direction');
  expect(direction.tagName).toBe('BUTTON');
  expect(direction.textContent).toBe('>');
});
