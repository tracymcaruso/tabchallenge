/* eslint-disable no-console */
import React from 'react';
import { render, cleanup } from 'react-testing-library';

import Content from 'Components/Content';

afterEach(() => {
  cleanup();
  console.error.mockClear();
});

// eslint-disable-next-line no-undef
console.error = jest.fn();

test('<Content />, without tab', () => {
  render(<Content />);
  expect(console.error).toHaveBeenCalled();
});

test('<Content />, with wrong props', () => {
  render(<Content activeTab={{ other: 'data' }} />);
  expect(console.error).toHaveBeenCalled();
});

const tab = { title: 'test 0', content: 'test content 0', id: 0, url: 'png.com' };
test('<Content />', () => {
  const { getByTestId } = render(<Content activeTab={tab} />);
  expect(getByTestId('content').textContent).toBe(tab.content);
});
