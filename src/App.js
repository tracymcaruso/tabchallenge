/* eslint react/no-did-mount-set-state: 0 */
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Tabs from 'Components/Tabs';

const App = () => (
  <Router>
    <div className="App">
      <Switch>
        <Route exact path="/" component={Tabs} />
      </Switch>
    </div>
  </Router>
);

export default App;
