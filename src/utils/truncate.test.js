import truncate from './truncate'

const TEXT =
  'This is a long line of text. Words word words and not to mention other words.';

test('truncate', () => {
  expect(truncate(TEXT, 25)).toBe('This is a long line of te');
});
