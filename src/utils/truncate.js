const truncate = (text, length) => {
  return text.substring(0, length);
};

export default truncate;
