module.exports = {
  parser: 'babel-eslint',
  env: {
    browser: true,
    es6: true,
  },
  settings: {
    ecmascript: 6,
    jsx: true,
    'import/resolver': {
      webpack: {
        config: './webpack.config.js',
      },
    },
  },
  parserOptions: {
    ecmaVersion: 2017,
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      experimentalDecorators: true,
      jsx: true,
    },
    sourceType: 'module',
  },
  plugins: ['react', 'prettier', 'jsx-a11y', 'security'],
  extends: [
    'airbnb',
    'prettier',
    'prettier/react',
    'plugin:jsx-a11y/recommended',
    'plugin:security/recommended',
  ],
  rules: {
    'react/jsx-filename-extension': 0,
    'function-paren-newline': 0,
    'import/no-unresolved': 0,
    'react/jsx-one-expression-per-line': 0,
  },
  globals: {
    test: true,
    expect: true,
    afterEach: true,
  },
};
