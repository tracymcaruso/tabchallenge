module.exports = {
  setupFilesAfterEnv: ['./setup-test-env.js'],
  moduleNameMapper: {
    'Components(.*)$': '<rootDir>/src/components/$1',
  },
};
